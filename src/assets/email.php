<?php

$post_data = file_get_contents("php://input");
$data = json_decode($post_data);
 
echo "Name : " . $data->name;
echo "Email : " . $data->email;
echo "Message : " . $data->message;
 
$to = $data->email;
 
$subject = 'Email from website: from ' . $data->name;
 
$message = $data->message;
 
$headers = 	'From: ' . $data->email . "\r\n" .
        	'Reply-To: '. $to . "\r\n" .
        	'X-Mailer: PHP/' . phpversion();

mail($to, $subject, $message, $headers);
 
?>