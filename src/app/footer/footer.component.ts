import { Component, OnInit } from '@angular/core';
import { MailService, IMessage } from '../mail.service';
import { BrowserModule } from '@angular/platform-browser';
import { ToasterModule, ToasterService, ToasterConfig, Toast } from 'angular2-toaster';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  private toasterService: ToasterService;
  public config1: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-bottom-full-width',
  });
  message: IMessage = {};
  constructor(private appService: MailService, toasterService: ToasterService) {
    this.toasterService = toasterService;
  }
  messageResponse: String;
  ngOnInit() {
  }
  sendEmail(message: IMessage) {
    this.appService.sendEmail(message).subscribe(res => {
      console.log('Mail was sent', res);
      this.messageResponse = 'E-mail was successfully sent.';
      this.popSuccessToast();
    }, error => {
      console.log('Mail failed', error);
      this.messageResponse = 'E-mail failed for some reason.';
      this.popFailureToast();
    })


  }
  popSuccessToast() {
    var toast: Toast = {
      type: 'success',
      body: this.messageResponse
    };
    this.toasterService.pop(toast);
  }
  popFailureToast() {
    var toast: Toast = {
      type: 'error',
      body: this.messageResponse
    };
    this.toasterService.pop(toast);
  }

}
