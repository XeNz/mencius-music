import { TestBed, inject } from '@angular/core/testing';

import { LatestUploadService } from './latest-upload.service';

describe('LatestUploadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LatestUploadService]
    });
  });

  it('should be created', inject([LatestUploadService], (service: LatestUploadService) => {
    expect(service).toBeTruthy();
  }));
});
