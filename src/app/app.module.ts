import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { MenubarComponent } from './menubar/menubar.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { LatestUploadPanelComponent } from './latest-upload-panel/latest-upload-panel.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ContactComponent } from './contact/contact.component';
import { SubmitComponent } from './submit/submit.component';
import { QuotesComponent } from './quotes/quotes.component';
import { AboutComponent } from './about/about.component';
import { ToasterModule } from 'angular2-toaster';
import { HttpModule } from '@angular/http';
import { LatestUploadService } from "./latest-upload.service";
import { MailService } from "./mail.service";

const appRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  { path: 'about', component: AboutComponent },
  { path: 'quotes', component: QuotesComponent },
  { path: 'submit', component: SubmitComponent },
  { path: 'contact', component: ContactComponent },
  {
    path: '',
    component: HomeComponent
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MenubarComponent,
    HomeComponent,
    FooterComponent,
    LatestUploadPanelComponent,
    PageNotFoundComponent,
    ContactComponent,
    SubmitComponent,
    QuotesComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ToasterModule,
    BrowserAnimationsModule,
    HttpModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [LatestUploadService, MailService],
  bootstrap: [AppComponent,]
})
export class AppModule { }
