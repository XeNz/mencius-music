import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LatestUploadPanelComponent } from './latest-upload-panel.component';

describe('LatestUploadPanelComponent', () => {
  let component: LatestUploadPanelComponent;
  let fixture: ComponentFixture<LatestUploadPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LatestUploadPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LatestUploadPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
