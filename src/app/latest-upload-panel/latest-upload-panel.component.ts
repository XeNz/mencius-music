import { Component, OnInit, Provider } from '@angular/core';
import { LatestUploadService } from '../latest-upload.service';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Upload } from '../upload';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-latest-upload-panel',
  templateUrl: './latest-upload-panel.component.html',
  styleUrls: ['./latest-upload-panel.component.css']
})

export class LatestUploadPanelComponent implements OnInit {

  constructor(private latestUploadService: LatestUploadService) {

  }
  public latestUploadList: Upload[] = [];

  ngOnInit() {
    this.getLatestUploads();
  }

  getLatestUploads() {
    this.latestUploadService.getLatestUploads().subscribe(data => {
      let count = 0;
      data.items.forEach(element => {
        try {
          if (count < 6) {
            const upload = new Upload();
            upload.title = element.snippet.title;
            upload.url = 'https://www.youtube.com/watch?v=' + element.snippet.resourceId.videoId;
            upload.thumbnail = element.snippet.thumbnails.maxres.url;
            this.latestUploadList.push(upload);
            console.log(upload);
            count++;
          }

        } catch (error) {
          console.log(error);
        }
      });
    });
  }
}
