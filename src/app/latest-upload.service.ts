import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';


@Injectable()
export class LatestUploadService {

  constructor(public http: Http) {
  }

  PLAYLIST_ID = 'UU8xpakpTlmn_Q8BPCNSSVZw';
  MAX_RESULTS = '10';
  API_KEY = 'AIzaSyB_P_VwozXo87SXLRGbMvLAojr639yedOU';

  URL = 'https://www.googleapis.com/youtube/v3/playlistItems?playlistId=' + this.PLAYLIST_ID + '&key=' + this.API_KEY + '&part=snippet&maxResults=' + this.MAX_RESULTS;

  getLatestUploads() {
    const response = this.http.get(this.URL).map(res => res.json());
    return response;
  }
}

