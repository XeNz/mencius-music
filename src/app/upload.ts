export class Upload {
  title: String;
  url: String;
  thumbnail: String;
}