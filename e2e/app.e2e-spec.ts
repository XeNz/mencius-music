import { MENCIUSMUSICPage } from './app.po';

describe('mencius-music App', () => {
  let page: MENCIUSMUSICPage;

  beforeEach(() => {
    page = new MENCIUSMUSICPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
